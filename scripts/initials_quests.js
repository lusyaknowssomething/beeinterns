// массив с карточками

const initialCards = [
  {
    title: 'React',
    subtitle: 'Назначение блоков: head, body' ,
    date: '01.02.2022',
    link: './images/podkluchenie-stiley.jpg'
  },
  {
    title: 'React',
    subtitle: 'Назначение блоков: head, body' ,
    date: '01.02.2022',
    link: './images/podkluchenie-stiley.jpg'
  },
  {
    title: 'React',
    subtitle: 'Назначение блоков: head, body' ,
    date: '01.02.2022',
    link: './images/CSS.jpg'
  },
  {
    title: 'JavaScript',
    subtitle: 'Развертывание проекта на Github/gitlab. Продвинутая работа с Github/gitlab' ,
    date: '12.04.2022',
    link: './images/struktura HTML.jpg'
  },
  {
    title: 'JavaScript',
    subtitle: 'Продвинутое использование Git (git rebase, git status, git diff, git reset, git cherry-pick, git stash, git revert, git fetch)' ,
    date: '14.04.2022',
    link: './images/podkluchenie-stiley.jpg'
  },
  {
    title: 'JavaScript',
    subtitle: 'Панель разработчика Chrome' ,
    date: '19.04.2022',
    link: './images/podkluchenie-stiley.jpg'
  },
  {
    title: 'JavaScript',
    subtitle: 'Введение в JavaScript' ,
    date: '21.04.2022',
    link: './images/CSS.jpg'
  },
  {
    title: 'JavaScript',
    subtitle: 'Типы данных. Условные операторы. Переменные/Константы' ,
    date: '26.04.2022',
    link: './images/struktura HTML.jpg'
  },
  {
    title: 'CSS',
    subtitle: 'Базовый синтаксис CSS: селекторы, классы' ,
    date: '01.02.2022',
    link: './images/struktura HTML.jpg'
  },
  {
    title: 'CSS',
    subtitle: 'Наследие и каскад' ,
    date: '01.02.2022',
    link: './images/podkluchenie-stiley.jpg'
  },
  {
    title: 'CSS',
    subtitle: 'Принципы современной верстки сайта на flex' ,
    date: '01.02.2022',
    link: './images/podkluchenie-stiley.jpg'
  },
  {
    title: 'CSS',
    subtitle: 'Адаптивная верстка. Использование media и viewport' ,
    date: '01.02.2022',
    link: './images/CSS.jpg'
  },
  {
    title: 'CSS',
    subtitle: 'CSS Grid' ,
    date: '01.02.2022',
    link: './images/CSS.jpg'
  },
  {
    title: 'HTML',
    subtitle: 'Назначение блоков: head, body' ,
    date: '01.02.2022',
    link: './images/struktura HTML.jpg'
  },
  {
    title: 'HTML',
    subtitle: 'Виды подключения стилей. Подключение шрифтов' ,
    date: '03.02.2022',
    link: './images/podkluchenie-stiley.jpg'
  },
  {
    title: 'HTML',
    subtitle: 'Назначение блоков: head, body' ,
    date: '01.02.2022',
    link: './images/podkluchenie-stiley.jpg'

  },
  {
    title: 'HTML',
    subtitle: 'Назначение блоков: head, body' ,
    date: '01.02.2022',
    link: './images/CSS.jpg'
  },
]
